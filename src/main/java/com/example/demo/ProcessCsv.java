package com.example.demo;

import com.example.demo.models.CSVBean;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;

public class ProcessCsv {
    public HashMap<String, String> beanBuilderExample(Path path, Class clazz) throws Exception {
        ColumnPositionMappingStrategy ms = new ColumnPositionMappingStrategy();
        ms.setType(clazz);

        Reader reader = Files.newBufferedReader(path);
        CsvToBean cb = new CsvToBeanBuilder(reader)
                .withType(clazz)
                .withMappingStrategy(ms)
                .build();

        List<CSVBean> list = cb.parse();
        HashMap<String,String> map = new HashMap<>();
        reader.close();
        for (CSVBean csv : list)
            map.put(csv.getExampleColOne(),csv.getExampleColTwo());
        return map;
    }

    public String simplePositionBeanExample() throws Exception {
        Path path = Paths.get(
                ClassLoader.getSystemResource("Sample.csv").toURI());
        return beanBuilderExample(path, CSVBean.class).toString();
    }
}
