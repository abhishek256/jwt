package com.example.demo.models;

import com.opencsv.bean.CsvBindByPosition;

public class CSVBean {
    @CsvBindByPosition(position = 0)
    private String exampleColOne;

    @CsvBindByPosition(position = 1)
    private String exampleColTwo;

    public CSVBean() {
    }

    public String getExampleColOne() {
        return exampleColOne;
    }

    public void setExampleColOne(String exampleColOne) {
        this.exampleColOne = exampleColOne;
    }

    public String getExampleColTwo() {
        return exampleColTwo;
    }

    public void setExampleColTwo(String exampleColTwo) {
        this.exampleColTwo = exampleColTwo;
    }
}
